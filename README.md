# Abuse Vectors: A Framework for Conceptualizing IoT-Enabled Interpersonal Abuse

This repository contains supporting content for the paper:

Sophie Stephenson, Majed Almansoori, Pardis Emami-Naeini, Danny Yuxing Huang, Rahul Chatterjee. Abuse Vectors: A Framework for Conceptualizing IoT-Enabled Interpersonal Abuse. In *32nd USENIX Security Symposium (USENIX Security 23)*, 2023.

Files:
- `codebook.pdf` contains our final codebook based on the analysis of web pages.
- `query_templates.py` contains the lists of devices, actors, and templates we combined to create our list of seed queries.
