northeastern_devices = [
	# cameras
	"Amazon Cloudcam",
	"Amcrest Cam",
	"Blink Cam",
	"Blink Hub",
	"Bosiwo Cam",
	"D-Link Cam",
	"Lefun Cam",
	"Luohe Cam",
	"Microseven Cam",
	"Ring Doorbell",
	"Wansview Cam",
	"WiMaker Spy Camera",
	"Xiaomi Cam",
	"Yi Cam",
	"ZModo Doorbell",

	# smart hubs
	"Insteon",
	"Lightify",
	"Philips Hue",
	"Sengled",
	"Smartthings",
	"Wink 2",
	"Xiamoi",

	# home automation
	"D-Link Mov Sensor",
	"Flux Bulb",
	"Honeywell Thermostat",
	"Magichome Strip",
	"Nest Thermostat",
	"Philips Bulb",
	"TP-Link Bulb",
	"TP-Link Plug",
	"WeMo Plug",
	"Xiaomi Strip",

	# tv
	"Apple TV",
	"Fire TV",
	"LG TV",
	"Roku TV", 
	"Samsung TV",

	# audio
	"Allure with Alexa",
	"Echo Dot",
	"Echo Spot", 
	"Echo Plus",
	"Google Home Mini",
	"Google Home",
	"Invoke with Cortana",

	# appliances
	"Anova Sousvide",
	"Behmor Brewer",
	"GE Microwave",
	"Netatmo Weather",
	"Samsung Dryer",
	"Samsung Fridge",
	"Samsung Washer",
	"Smarter Brewer",
	"Smarter iKettle",
	"Xiaomi Cleaner",
	"Xiaomi Rice Cooker"

]

additional_devices = [
	# specific devices
	"Apple Watch", 
	"FitBit",
	"AirTags",
	"Alexa",
	"IFTTT",
	"AirPods",
	"Ring doorbell",

	# generic terms
	"smart home", 
	"home automation",
	"webcam",
	"smart camera",
	"security camera",
	"smart thermostat",
	"wifi thermostat",
	"smart speaker",
	"smart appliances",
	"smart lock",
	"smart doorbell",
	"smart light",
	"smart watch",
	"smart tv",
	"baby monitor",
	"wireless headphones",
]

DEVICES = northeastern_devices + additional_devices

AGENTS = [
	"wife",
	"husband",
	"boyfriend",
	"girlfriend",
	"partner",
	"spouse",
	"roommate",
	"ex",
]

TEMPLATES = [
	# spying
	"Spy on [agent] using [device]",
	"Track [agent] with [device]", 
	"Spying on me with [device]",
	"Stalk [agent] with [device]",
	"Access recordings on [agent]'s [device]",
	"Listen to [agent] with [device]",
	"Eavesdrop on [agent] with [device]",
	"Monitor surroundings with [device]",
	"[device] listen to sounds at home",
	"Secretly record [agent] with [device]",
	"Keep tabs on [agent] with [device]",
	"Webcam to see if [agent] leaves the house",
	"Track [agent]'s location with [device]",
	"View [agent]'s \"smart doorbell\" footage",
	"Watch what [agent] is doing with [device]",
	"See [agent]'s movement in the house with [device]",
	"Use [device] for surveillance",
	"Look at history on [agent]'s [device]",
	"Use [device] to drop in on household conversation",
	"Use [device] to snoop without them knowing",
	"Can [device] be set up to listen at home by my [agent]",
	"Use [device] to check up on my [agent]",

	# harassment
	"Troll [agent] via [device]",
	"Mess with [agent] [device]",
	"Get revenge on [agent] with [device]",
	"Lock [agent] out of \"smart home\"",
	"Trap [agent] using [device]",
	"Use [device] to scare [agent]",
	"Use [device] to harass [agent]",
	"Use [device] to intimidate [agent]",
	"[device] psychological abuse",
	"Tamper with [agent]'s [device] from afar",
	"Switch on [agent] \"smart lights\" at night",
	"Change \"smart lock\" on [agent]'s door",
	"Enter [agent] \"smart home\" without them knowing",
	"Use [device] to establish power over [agent]",


	# infidelity
	"Use [device] to listen for cheating [agent]",
	"[device] helps catch cheating [agent]",
	"Buy [agent] [device] to catch them in the act",
	"Uncover [agent]'s infidelity with [device]",


	# general
	"[device] used for domestic violence",
	"[agent] installed secret [device]",
	"Ex-[agent] remains in smart home",
	"Leverage [device] against [agent]",
	"Exploit [agent]'s [device]",
	"[device] turned against [agent]",
	"[device] used against someone",
	"Turn off [agent]'s [device] from a remote location",
	"Abuse [agent] via [device]",
	"Tamper with [agent]'s [device] from afar",
	"Abuse via the internet of things",
]
